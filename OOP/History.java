public class History {

    public interface HConstants {
        public enum StateType {PAST, CURRENT, FUTURE}
    }

    //Inner class
    public class State implements HConstants {
        public StateType stateField = StateType.PAST;



    }

    public static void main(String[] args) {

        //class hello{}
        HConstants.StateType stateField0 = HConstants.StateType.PAST;
        HConstants.StateType stateField1 = HConstants.StateType.CURRENT;
        HConstants.StateType stateField2 = HConstants.StateType.FUTURE;

        System.out.println(stateField0);
        System.out.println(stateField1);
        System.out.println(stateField2);

    }



}
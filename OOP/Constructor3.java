public class Constructor3 {
    public Constructor3() {
        System.out.println("Running no-args constructor");
    }

    public Constructor3(String name,String surname) {
        this();
        System.out.println("Name = " + name);
        System.out.println("Surname = "+surname);
    }

}



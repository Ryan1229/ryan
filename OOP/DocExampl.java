public class DocExampl {

    /**Variable javadoc*/

    protected int var;

    /**Method level javadoc*/
    public int getVar(){
        return var;
    }
}

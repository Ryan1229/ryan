class ObjectA {
    public static void main(String[] args) {
        ObjectB objectB = new ObjectB();

        objectB.add(1, 2,3);

    }
}

class ObjectB {
    public void add(int val1,int val2,int val3){
        int sum = val1+val2;
        System.out.println("val1 + val2 = "+sum);
    }
}


/* with regards to this code this was just to see what the difference between the if-then if-then-else and the if-then-else-if
statements and how each of them work and how each of these statements function each of these statements are assessed by the compiler */
public class TheIfStatement {
    static int x =12;
    
    public static void main(String[] args) {
        x-=2;
// if Statement
        if (x<10){
            System.out.println("True");
        }


// if-then-else statement

        if (x<10){
            System.out.println("True");

        }else{
            System.out.println("False");
        }

// if-then-else-if statement

        if (x<10){
            System.out.println("True");

        }
        else if (x>10){
            System.out.println("X is greater than 10 ");
        }
        else if (x==10){
            System.out.println("X is equal to 10");
        }
    }
}
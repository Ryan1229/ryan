public class BinTest {
	public static void main(String[] args) {
		int bina = -5;
		int binb = 15;
				
		System.out.println("Binary unshifted a (-5): "+Integer.toBinaryString(bina));
		System.out.println("Binary   shifted a (-5): "+Integer.toBinaryString(bina >> 2));

		System.out.println("Binary unshifted b (15): "+Integer.toBinaryString(binb));
		System.out.println("Binary   shifted b (15) "+Integer.toBinaryString(binb >> -2));
	}
}


public class BreakTestDowhile {
    public static void main(String[] args) {
        int i=0;
        int limit = 7;

      do {
          System.out.println("Counter = " + i);
          if(i++ == limit){break;}
      } while (i<10);

    }
}


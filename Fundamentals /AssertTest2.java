public class AssertTest2{
    public static void main(String[] args) {
        int a = 1;
        int b = 4;
        boolean condition1= a>0;
        boolean condition2= b>10;

        assert condition1: "a must be greater than 0";
        assert condition2: "b must be greater than 10";
        System.out.println("Finished");

    }
}

public class BlockTest2 {
    public static void main(String[] args) {
        int a = 0;
        {
            int i =1;
            a=i;
            System.out.println("Block 1 i = "+ i);
        }

        {
            int i =1;
            a+=i;
            System.out.println("Block 2 i = "+ i);
        }
        System.out.println("BlockSum = " +a);
    }
}

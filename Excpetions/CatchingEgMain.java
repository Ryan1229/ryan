public class CatchingEgMain {
    public static void main(String[] args) {
        try{
            CatchingEg r = new CatchingEg();
            r.doSomething();
        }catch(Throwable e){
            e.printStackTrace();
        }
    }
}

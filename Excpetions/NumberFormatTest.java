public class NumberFormatTest {
    public static void main(String[] args) {
try {
    String number = "2345Q";

    System.out.println("Step 1");

    int converted = Integer.parseInt(number);

    System.out.println("Step 2");

}catch (NumberFormatException e){
    System.out.println(e.getMessage());
}
    }
}

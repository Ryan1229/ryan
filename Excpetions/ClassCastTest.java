public class ClassCastTest {
    public static void main(String[] args) {
try{
        Object exception = new Object();

        System.out.println("Step 1");

        Exception exception2 = (Exception)exception;

        System.out.println("Step 2");

}catch(ClassCastException e){
    System.out.println(e.getMessage());
}
    }
}



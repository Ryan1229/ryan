public class CatchingEg3 {

    public void doSomething()throws RyanException{
        int i = 35;

        try {

            if (i > 30) {
                System.out.println("An Error has occurred");
                throw new RyanException("An error occurred");

            }

        }catch (Exception e){
            System.out.println("Handled");
        }
        System.out.println("Doing something...");
    }


}

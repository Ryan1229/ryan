public class MyException {

    static int [][] a = {{1,2,4},null,{4,5,6},null};

    public static void main(String[] args) {
        System.out.println("Start");
        try{
            System.out.println("Null Pointer Thrown");
            System.out.println(a[1].toString());

        } catch (NullPointerException e){
            System.out.println("Exception Handled");
        }
    }
}
